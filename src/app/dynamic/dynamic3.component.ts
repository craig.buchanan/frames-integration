import {Component}  from '@angular/core';
import {DataHolder} from './data-holder';

@Component({
  template: `
                <p>This is component 3.</p>
                <p>Dataholder value: {{dh.value}}</p>
                <p>Dataholder pingpong: {{dh.pingPong}}</p>
                <button (click)="dh.ping()">ping</button>
            `
})
export class Dynamic2Component {
  constructor(public dh: DataHolder) {
  }

}
