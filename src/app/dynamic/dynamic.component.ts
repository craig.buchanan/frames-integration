import { Component, OnInit } from '@angular/core';
import {DataHolder}          from './data-holder';

@Component({
  selector: 'mk-dynamic',
  template: `
                <p>This is component 1.</p>
                <p>Dataholder value: {{dh.value}}</p>
                <p>Dataholder pingpong: {{dh.pingPong}}</p>
                <button (click)="dh.pong()">pong</button>
            `,
  styleUrls: ['./dynamic.component.styl']
})
export class DynamicComponent implements OnInit {

  constructor(public dh: DataHolder) { }

  ngOnInit(): void {
  }

}
