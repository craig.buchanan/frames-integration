export class DataHolder {
  private pValue = 0;
  private pPing  = true;

  get value() { return this.pValue; }

  add() { return this.pValue++; }

  get pingPong() { return this.pPing ? 'ping' : 'pong'; }

  ping() { this.pPing = true; }

  pong() { this.pPing = false; }
}
