import {Component}  from '@angular/core';
import {DataHolder} from './data-holder';

@Component({
  template: '<p>This is component 2.</p><p>Dataholder value: {{dh.value}}</p>'
})
export class Dynamic2Component {
  constructor(public dh: DataHolder) {
  }

}
