import {Component, ComponentFactoryResolver, NgZone, ViewChild, ViewContainerRef} from '@angular/core';
import {DataHolder}                                                               from './dynamic/data-holder';

declare const window: Window;


const myImports = {
  target1: async () => (await import('./dynamic/dynamic.component'))['DynamicComponent'],
  target2: async () => (await import('./dynamic/dynamic2.component'))['Dynamic2Component'],
  target3: async () => (await import('./dynamic/dynamic3.component'))['Dynamic2Component'],
};

@Component({
  selector:    'mk-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.styl']
})
export class AppComponent {

  @ViewChild('target', {read: ViewContainerRef}) target: ViewContainerRef;
  title = 'frames-integration';
  delay = 1;

  constructor(private cfr: ComponentFactoryResolver, private vcr: ViewContainerRef, public dh: DataHolder, private zone: NgZone) {
    /* tslint:ignore-next */
    window['myAppComponentLoader'] = (...values: string[]) => this.registerComponentTarget(values);
  }

  async registerComponentTarget(targetNames: string[]) {
    console.log(' register component was called with: ', targetNames);
    const waiter = t => new Promise(r => setTimeout(r, t));
    let counter  = 0;
    this.zone.run(async () => {
      while (counter < targetNames.length) {

        this.renderComponent(targetNames[counter]);
        await waiter(this.delay * 1000);
        counter++;
      }
    });
  }

  add() {
    this.dh.add();
  }

  async renderComponent(key: string): Promise<void> {

    const cmp = await myImports[key]();

    const target = this.target.element.nativeElement.contentDocument.getElementById(key);

    const cmpFac                      = this.cfr.resolveComponentFactory(cmp);
    const compRef                     = this.vcr.createComponent(cmpFac);
    compRef.location.nativeElement.id = 'innerComp';
    target.innerHTML                  = '';
    target.appendChild(compRef.location.nativeElement);
  }
}
