import {BrowserModule} from '@angular/platform-browser';
import {NgModule}      from '@angular/core';
import {AppComponent}  from './app.component';
import {DataHolder}    from './dynamic/data-holder';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports:      [
    BrowserModule
  ],
  providers:    [DataHolder],
  bootstrap:    [AppComponent]
})
export class AppModule {}
